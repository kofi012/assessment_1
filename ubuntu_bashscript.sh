# prompts user to enter ip
read -p "Please enter IP address: " 

# if statement used to check if user has input ip then outputs welcome message, if not then re-prompt
if [ -z "$REPLY" ]
    then
        echo "Please enter an IP address"
        exec $0
    else
        echo "Welcome $REPLY"
fi

# connecting to remote server using key and user-inputted ip
ssh -o StrictHostKeyChecking=no -i ~/.ssh/Kofi-key-AWS.pem ubuntu@$REPLY '

# once in virtual machine, this updates all programs installed

sudo apt update -y

# this if statement checks to see if nginx is installed, then installs if

if [ -z $(apt list --installed | grep nginx)] > /dev/null 2>&1
then
sudo apt install nginx -y
else
echo "nginx is already installed"
fi


sudo sh -c "cat >/var/www/html/index.html <<_END_
<html>
<head>
<title> Welcome to nginx </title>
</head>
<body>
<h1>Kofi</h1>
<p> Welcome to my edited version of nginx </p>
</body>
</html>
_END_"

sudo systemctl restart nginx
'