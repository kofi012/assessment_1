# prompts user to enter ip
read -p "Please enter IP address: " 

# if statement used to check if user has input ip then outputs welcome message, if not then re-prompt
if [ -z "$REPLY" ]
    then
        echo "Please enter an IP address"
        exec $0
    else
        echo "Welcome $REPLY"
fi

# connecting to remote server using key and user-inputted ip
ssh -o StrictHostKeyChecking=no -i ~/.ssh/Kofi-key-AWS.pem ec2-user@$REPLY '

# once in virtual machine, this updates all programs installed

sudo yum update -y

sudo amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2

# this if statement checks to see if httpd is installed, then installs if

if [ -z $(yum list httpd)] > /dev/null 2>&1
then
sudo yum install httpd -y
else
echo "httpd is already installed"
fi

sudo sh -c "cat >/var/www/html/index.html <<_END_
<html>
<head>
<title> Welcome to Apache </title>
</head>
<body>
<h1>Kofi</h1>
<p> Welcome to my edited version of apache </p>
</body>
</html>
_END_"

sudo systemctl restart httpd

sudo systemctl enable httpd

'