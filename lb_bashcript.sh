# prompts user to enter ip
read -p "Please enter IP address: " 

# if statement used to check if user has input ip then outputs welcome message, if not then re-prompt
if [ -z "$REPLY" ]
    then
        echo "Please enter an IP address"
        exec $0
    else
        echo "Welcome $REPLY"
fi

# connecting to remote server using key and user-inputted ip
ssh -o StrictHostKeyChecking=no -i ~/.ssh/Kofi-key-AWS.pem ubuntu@$REPLY '

# once in virtual machine, this updates all programs installed

sudo apt show haproxy

sudo add-apt-repository ppa:vbernat/haproxy-1.7

sudo apt update

if [ -z $(apt list --installed | grep haproxy)] > /dev/null 2>&1
then
sudo apt install -y haproxy
else
echo "haproxy is already installed"
fi


sudo sh -c "cat >>/etc/haproxy/haproxy.cfg <<_END_
frontend http_front
   bind *:80
   stats uri /haproxy?stats
   default_backend http_back

backend http_back
   balance roundrobin
   server kofi-ubuntu 172.31.37.157:80 check
   server kofi-AWS 172.31.47.27:80 check
_END_"

sudo systemctl restart haproxy
'